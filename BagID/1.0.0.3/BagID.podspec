Pod::Spec.new do |s|

  s.name                = "BagID"
  s.version             = "1.0.0.3"
  s.summary             = "----"
  s.description         = "---- ---- ----"
  s.homepage            = "http://bagid.com"
  s.license             = "MIT"
  s.author              = "-----"
  s.platform            = :ios, '9.0'
  s.source              = { :http => 'https://www.dropbox.com/s/q3u5bvcrrpt0le1/BagID.zip'}
  s.swift_version       = "4.2"
  s.ios.vendored_frameworks = 'BagID.framework'
end
