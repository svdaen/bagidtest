Pod::Spec.new do |s|

  s.name                = "BagID"
  s.version             = "1.0.0.2"
  s.summary             = "----"
  s.description         = "---- ---- ----"
  s.homepage            = "http://bagid.com"
  s.license             = "MIT"
  s.author              = "-----"
  s.platform            = :ios, "9.0"
  s.source              = { :https => 'https://www.dropbox.com/s/zxw8qswk3md024f/BagID.zip?dl=0', :tag => s.version }
  s.swift_version       = "4.2"
  s.ios.vendored_frameworks = 'BagID.framework'
end
